clear;
clc;

addpath('functions')
% Get the folder of all data
upper_folder = uigetdir; 
    
% List all subject folder
subject_list = dir(upper_folder);
subject_list = subject_list([subject_list(:).isdir]==1);
subject_list = subject_list(~ismember({subject_list.name},{'.','..'}));

n_electrodes = 14;
sampling_rate = 128;
frequency_to_keep = 50;
bands_list = [1,4,4,8,8,12,12,20];

% Create the first column of output table
% first_column = cell(length(subject_list),1);
% for n_subject = 1:length(subject_list)
%     first_column{n_subject} = subject_list(n_subject).name;
% end
% first_column = [{'Subject'} ; first_column];



% Create header of frequency output file
% header_frequency = {'V1', '','','', 'V1_1', '','','', 'V1_2', '','','', 'V1_3', '','','', 'V2', '','','',... 
%     'V2_1', '','','', 'V2_3', '','','', 'PV4', '','','',... 
%     'PV5', '','','', 'PV6', '','','', 'PV7', '','','', 'PV8', '','','', 'PV9', '','','', 'PV10', '','','',... 
%     'PV11', '',''};
% 
% delta_outputfile = header_frequency;
% theta_outputfile = header_frequency;
% alpha_outputfile = header_frequency;
% beta_outputfile = header_frequency;


% Create header of arousal output file
% header_arousal = {'V4', '','','', 'A1', '','','', 'A1_1', '',''};
% arousal_outputfile = header_arousal;

if ~exist('output_folder_emotiv')
    mkdir('output_folder_emotiv')
end

% Creat first column of all elctrode
first_column = cell(n_electrodes,1);
for i = 1:n_electrodes
    first_column{i} = ['e_' num2str(i)];
end

% Creat header
header = [{'Electrode'}, {'Neutre-Faible'}, {'Intense'}, {'Frisson'}];

for subject = 1:length(subject_list)
    subject_name = subject_list(subject).name;
    subject_path = strcat(upper_folder,filesep,subject_name);
    condition_list = dir(subject_path);
    condition_list = condition_list([condition_list(:).isdir]==1);
    condition_list = condition_list(~ismember({condition_list.name},{'.','..'}));
    
    % for each condition
    psd_notseparated = [];
    for condition = 1:length(condition_list)
        condition_name = condition_list(condition).name;
        condition_path = strcat(subject_path,filesep,condition_name);
        
        epoch_list = dir(fullfile(condition_path, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));
        
        psd_matrix = spectralComputation1SecRegressZeropadded(epoch_list,n_electrodes,sampling_rate,frequency_to_keep);
        psd_notseparated = [psd_notseparated; powerbandsAnalysis(psd_matrix, bands_list)];
        disp(['finish subject ' num2str(subject) ' :condition ' num2str(condition)])
    end
    
    faible_psd = psd_notseparated(1:4,:); % matrix of [bands x electrodes]
    intense_psd = psd_notseparated(5:8,:);
    frisson_psd = psd_notseparated(9:12,:);
    
    bands_name = [{'Delta'},{'Theta'},{'Alpha'},{'Beta'}];
    for nbands = 1:4
        data_psd = faible_psd(nbands,:)';
        data_psd = [data_psd intense_psd(nbands,:)'];
        data_psd = [data_psd frisson_psd(nbands,:)'];
        data_psd = num2cell(data_psd);
        data_to_write = [header; [first_column data_psd]];
        writetable(cell2table(data_to_write),['output_folder_emotiv' filesep subject_name '.xlsx'],'Sheet',bands_name{nbands},'FileType','spreadsheet','WriteVariableNames',false);
    end
    
%     delta = calculateFrequencyOutputFileEmotiv(faible_psd(1,:), intense_psd(1,:), frisson_psd(1,:));
%     theta = calculateFrequencyOutputFileEmotiv(faible_psd(2,:), intense_psd(2,:), frisson_psd(2,:));
%     alpha = calculateFrequencyOutputFileEmotiv(faible_psd(3,:), intense_psd(3,:), frisson_psd(3,:));
%     beta  = calculateFrequencyOutputFileEmotiv(faible_psd(4,:), intense_psd(4,:), frisson_psd(4,:));
%     arousal = calculateArousalOutputFileEmotiv(faible_psd(3:4,:), intense_psd(3:4,:), frisson_psd(3:4,:));
%     
%     delta_outputfile = [delta_outputfile ; delta];
%     theta_outputfile = [theta_outputfile ; theta];
%     alpha_outputfile = [alpha_outputfile ; alpha];
%     beta_outputfile = [beta_outputfile ; beta];
%     arousal_outputfile = [arousal_outputfile ; arousal];
    
end

% delta_outputfile = [first_column delta_outputfile];
% theta_outputfile = [first_column theta_outputfile];
% alpha_outputfile = [first_column alpha_outputfile];
% beta_outputfile = [first_column beta_outputfile];
% arousal_outputfile = [first_column arousal_outputfile];
% 
% writetable(cell2table(delta_outputfile),'result_Emotiv.xlsx','Sheet','Delta','FileType','spreadsheet','WriteVariableNames',false);
% writetable(cell2table(theta_outputfile),'result_Emotiv.xlsx','Sheet','Theta','FileType','spreadsheet','WriteVariableNames',false);
% writetable(cell2table(alpha_outputfile),'result_Emotiv.xlsx','Sheet','Alpha','FileType','spreadsheet','WriteVariableNames',false);
% writetable(cell2table(beta_outputfile),'result_Emotiv.xlsx','Sheet','Beta','FileType','spreadsheet','WriteVariableNames',false);
% writetable(cell2table(arousal_outputfile),'result_Emotiv.xlsx','Sheet','Arousal','FileType','spreadsheet','WriteVariableNames',false);