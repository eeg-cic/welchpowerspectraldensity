# Welch Power Spectral Density

This program compute the power spectral density for each subject, each bands, and each electrode.  
EGI - 257 electrode (1 sec epoch zeropadded to 4 sec)  
gtec - 8 electrode (1 sec epoch zeropadded to 4 sec)  
emotive - 14 electrode (regress eyeblink, 1 sec epoch zeropadded to 4 sec)

## Input
The program will ask user to choose a folder. There are seperated main file for each devices, please make sure to choose accordingly.

Intput: 1 folder for each device with this structure.
  
`emotive_data_folder`  
|  
|&ndash;&ndash;&ndash;&ndash; `AB01_subject_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `1_NeutreFaible_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch3.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `2_Intense_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch3.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `3_Frisson_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch3.eph`  

## Output
The program will generate a folder, and in the folder there will be excel files for each subject. one excel file contain welch power spectral dnsity value for each elctrode and condition. 

| Electrode | Neutre-Faible | Intense | Frisson |
| --------- | -----:| -----:| -----:|
| e_1  | 1 | 1 | 1 |
| e_2     |   1 | 1 | 1 |
| e_3      |    1 | 1 | 1 |

[-NOTE: First 3 sheet should be empty, the data begin on sheet 4 named Delta.-]
