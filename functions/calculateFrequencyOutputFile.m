function [output] = calculateFrequencyOutputFile( faible, intense, frisson )
    % CALCULATEFREQUENCYOUTPUTFILE
    % This function calculate all valence and powervalue for each condition
    % input is 3 vector for each condition of the same bands
    % EX: faible(alpha) , intense(alpha) , frisson(alpha)
    
    % Output is a 1-row vector of 1 bands for 1 subject
    % Ex: V1_faible, V1_intense, V1_frisson, V1_1_faible V1_1_intense, V1_1_frisson
    
    
    all_condition = [faible;intense;frisson];
    [row, ~] = size(all_condition);
    
    output = {};
    V1 = {};
    V1_1 = {};
    V1_2 = {};
    V1_3 = {};
    V2 = {};
    V2_1 = {};
    V2_3 = {};
    V3 = {};
    V3_1 = {};
    V3_2 = {};     
    PV1 = {};
    PV1_1 = {};
    PV1_2 = {};
    PV2 = {};
    PV2_1 = {};
    PV2_2 = {};
    PV3 = {};
    PV3_1 = {};
    PV3_2 = {};
    PV4 = {};
    PV5 = {};
    PV6 = {};
    PV7 = {};
    PV8 = {};
    PV9 = {};
    PV10 = {};
    PV11 = {};
        
    for i=1:row 
        AF3 = all_condition(i,34);
        AF4 = all_condition(i,12);
        F3 = all_condition(i,36);
        F4 = all_condition(i,224);
        F7 = all_condition(i,47);
        F8 = all_condition(i,2);
        FC5 = all_condition(i,49);
        FC6 = all_condition(i,213);
        
        ROIcg = mean([all_condition(i,9) all_condition(i,17) all_condition(i,43) all_condition(i,44)...
            all_condition(i,45) all_condition(i,51) all_condition(i,52) all_condition(i,53)...
            all_condition(i,80) all_condition(i,59) all_condition(i,60)]);
        ROIcd = mean([all_condition(i,198) all_condition(i,197) all_condition(i,196) all_condition(i,184)...
            all_condition(i,185) all_condition(i,186) all_condition(i,132) all_condition(i,144)...
            all_condition(i,155) all_condition(i,131) all_condition(i,183)]);
        ROIc = mean([all_condition(i,9) all_condition(i,17) all_condition(i,43) all_condition(i,44)...
            all_condition(i,45) all_condition(i,51) all_condition(i,52) all_condition(i,53)...
            all_condition(i,80) all_condition(i,59) all_condition(i,60) all_condition(i,198)... 
            all_condition(i,197) all_condition(i,196) all_condition(i,184) all_condition(i,185)... 
            all_condition(i,186) all_condition(i,132) all_condition(i,144) all_condition(i,155)... 
            all_condition(i,131) all_condition(i,183) all_condition(i,8) all_condition(i,81)... 
            all_condition(i,90)]);
        
        ROIpfg = mean([all_condition(i,20) all_condition(i,10) all_condition(i,11) all_condition(i,12)...
            all_condition(i,13) all_condition(i,2) all_condition(i,3) all_condition(i,4)...
            all_condition(i,5) all_condition(i,18) all_condition(i,19) all_condition(i,25)]);
        ROIpfd = mean([all_condition(i,46) all_condition(i,47) all_condition(i,37) all_condition(i,38)...
            all_condition(i,39) all_condition(i,35) all_condition(i,34) all_condition(i,33)...
            all_condition(i,32) all_condition(i,28) all_condition(i,27) all_condition(i,29)]);
        ROIpf = mean([all_condition(i,20) all_condition(i,10) all_condition(i,11) all_condition(i,12)...
            all_condition(i,13) all_condition(i,2) all_condition(i,3) all_condition(i,4)...
            all_condition(i,5) all_condition(i,18) all_condition(i,19) all_condition(i,25)...
            all_condition(i,46) all_condition(i,47) all_condition(i,37) all_condition(i,38)...
            all_condition(i,39) all_condition(i,35) all_condition(i,34) all_condition(i,33)...
            all_condition(i,32) all_condition(i,28) all_condition(i,27) all_condition(i,29)...
            all_condition(i,31) all_condition(i,26) all_condition(i,21)]);
        
        ROIfg = mean([all_condition(i,28) all_condition(i,29) all_condition(i,35) all_condition(i,23)...
            all_condition(i,30) all_condition(i,41) all_condition(i,49) all_condition(i,36)...
            all_condition(i,40) all_condition(i,48)]);
        ROIfd = mean([all_condition(i,213) all_condition(i,214) all_condition(i,215) all_condition(i,6)...
            all_condition(i,222) all_condition(i,13) all_condition(i,223) all_condition(i,224)...
            all_condition(i,5) all_condition(i,4)]);
        ROIf = mean([all_condition(i,28) all_condition(i,29) all_condition(i,35) all_condition(i,23)...
            all_condition(i,30) all_condition(i,41) all_condition(i,49) all_condition(i,36)...
            all_condition(i,40) all_condition(i,48)...
            all_condition(i,213) all_condition(i,214) all_condition(i,215) all_condition(i,6)...
            all_condition(i,222) all_condition(i,13) all_condition(i,223) all_condition(i,224)...
            all_condition(i,5) all_condition(i,4) all_condition(i,14) all_condition(i,15)...
            all_condition(i,21) all_condition(i,22)]);
        
        
        V1 = [V1 F4-F3];
    
        V1_1 = [V1_1 AF4-AF3];
    
        V1_2 = [V1_2 F8-F7];
        
        V1_3 = [V1_3 FC6-FC5];
        
        V2 = [V2 ((F4+AF4)/2)-((F3+AF3)/2)];
        
        V2_1 = [V2_1 ((F4+AF4+FC6)/3)-((F3+AF3+FC5)/3)];
        
        V2_3 = [V2_3 ((F4+AF4+FC6+F8)/4)-((F3+AF3+FC5+F7)/4)];
        
        V3 = [V3 ROIcd-ROIcg];
        
        V3_1 = [V3_1 ROIfd-ROIfg];
        
        V3_2 = [V3_2 ROIpfd-ROIpfg];      
        
        PV1 = [PV1 ROIcg];
        
        PV1_1 = [PV1_1 ROIcd];
        
        PV1_2 = [PV1_2 ROIc];
        
        PV2 = [PV2 ROIpfg];
        
        PV2_1 = [PV2_1 ROIpfd];
        
        PV2_2 = [PV2_2 ROIpf];
        
        PV3 = [PV3 ROIfg];
        
        PV3_1 = [PV3_1 ROIfd];
        
        PV3_2 = [PV3_2 ROIf];
        
        PV4 = [PV4 F3];
        
        PV5 = [PV5 F4];
        
        PV6 = [PV6 AF3];
        
        PV7 = [PV7 AF4];
        
        PV8 = [PV8 FC5];
        
        PV9 = [PV9 FC6];
        
        PV10 = [PV10 F7];
        
        PV11 = [PV11 F8];
        
    end
    output = [output, V1,{''}, V1_1,{''}, V1_2,{''}, V1_3,{''}, V2,{''}, V2_1,{''}, V2_3,{''}, V3,{''}, V3_1,{''}, V3_2,{''},...
            PV1,{''}, PV1_1,{''}, PV1_2,{''}, PV2,{''}, PV2_1,{''}, PV2_2,{''}, PV3,{''}, PV3_1,{''}, PV3_2,{''}, PV4,{''}, PV5,{''}, PV6,{''},...
            PV7,{''}, PV8,{''}, PV9,{''}, PV10,{''}, PV11];
        
end

