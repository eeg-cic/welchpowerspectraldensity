function output = spectralComputation1SecRegressZeropadded(epochlist,nelec,samplingrate,frequencytokeep)
% Give a matrix of PSD analysis 
% 1second regress with eye movement and zeropadded into 4 sec
    n_topad = (samplingrate*3)/2;
    hanning_window = hann(samplingrate);
    output = [];
    % for each epoch
    for epoch = 1:length(epochlist)
        
        [~,~,~,signal] = openeph(epochlist{epoch});
        % doing regression
        for i = 1:14
            signal(:,i) = signal(:,i)-mean(signal(:,i));
        end
        eye = [signal(:,2)-signal(:,13) signal(:,14)-signal(:,13)];
        [signal_coeff,signal_pca,~,~,~,~] = pca(signal, 'Economy',false);
        [~,eye_pca,~,~,~,~] = pca(eye, 'Economy', false);
        X = eye_pca;
        [~,~,Epca,~,~] = mvregress(X,signal_pca,'algorithm','cwls');
        regressed_signal = Epca/signal_coeff;

        % for each electrode
        for electrode = 1:nelec
            signal_padded = padarray(regressed_signal(:,electrode),n_topad);
            [pxx1,~] = pwelch(signal_padded,hanning_window,samplingrate/2,samplingrate,samplingrate);
            eachepoch(1,:,electrode) = pxx1(1:frequencytokeep);
        end % for each electrode
        
        output = [output ; eachepoch];
   
    end % for each epoch
end

