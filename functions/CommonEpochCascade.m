function [ x,y ] = CommonEpochCascade( folder1, folder2 )
%   This function receive 2 folder path and return 2 signal x and y 
%   x and y are concatenated .eph file of the common epoch in each folder
    
    flag = readtable([folder1 filesep 'flag.csv']);
    flag = table2cell(flag);
    epoch_array = cell(length(flag),4);
    epoch_array(:,1) = flag;

    epoch_list = dir(fullfile(folder1, '*.eph'));
    epoch_list = struct2cell(epoch_list);
    epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

    epoch_to_add = 1;
    for i = 1:length(flag)
        if epoch_array{i,1} == 1 
            epoch_array{i,2} = epoch_list{epoch_to_add};
        end
    end

    flag = readtable([folder2 filesep 'flag.csv']);
    flag = table2cell(flag);
    epoch_array(:,3) = flag;

    epoch_list = dir(fullfile(folder2, '*.eph'));
    epoch_list = struct2cell(epoch_list);
    epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

    epoch_to_add = 1;
    for i = 1:length(flag)
        if epoch_array{i,3} == 1 
            epoch_array{i,4} = epoch_list{epoch_to_add};
        end
    end

    
    epoch_to_use = [];
    for i = 1:length(flag)
        if epoch_array{i,1} && epoch_array{i,3} 
            epoch_to_use = [epoch_to_use i];
        end
    end

    x = [];
    for i = 1:length(epoch_to_use)
        [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),2});
        x = [x ; signal];
    end

    y = [];
    for i = 1:length(epoch_to_use)
        [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),4});
        y = [y ; signal];
    end
    
    
end

