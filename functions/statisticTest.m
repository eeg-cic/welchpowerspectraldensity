function output_args = statisticTest( data )
    % Perform statistic procedure
    % - Repeated ANOVA
    % - Friedman
    % - Post Hoc bonferroni, lsd, tukey
    % - Signed Rank Wilcoxon
    
    empty_table = cell(22,3);
    % Populating empty table
    empty_table(2,:) = {'RAnova F', 'df', 'p'};
    empty_table(4,:) = {'Friedman F', 'df', 'p'};
    empty_table(6,:) = {'Post Hoc', 'Bonferonni', ''};
    empty_table(7,:) = {'', 'B', 'C'};
    empty_table(8,:) = {'A', '', ''};
    empty_table(9,:) = {'B', '', ''};


end

