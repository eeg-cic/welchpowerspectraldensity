function [ output ] = calculateArousalOutputFileGtec( faible, intense, frisson )
all_condition = [faible;intense;frisson];
    [row, ~] = size(all_condition);

    output = {};
    V4 = {};
    A1 = {};
    
    for i = 1:2:row
        aAF3 = all_condition(i,5);
        aAF4 = all_condition(i,6);
        aF3 = all_condition(i,3);
        aF4 = all_condition(i,4);
        aF7 = all_condition(i,2);
        aF8 = all_condition(i,1);

        
        bAF3 = all_condition(i+1,5);
        bAF4 = all_condition(i+1,6);
        bF3 = all_condition(i+1,3);
        bF4 = all_condition(i+1,4);
        bF7 = all_condition(i+1,2);
        bF8 = all_condition(i+1,1);


        
        V4 = [V4 (aF4/bF4)-(aF3/bF3)];
        A1 = [A1 (bAF3+bAF4+bF3+bF4)/(aAF3+aAF4+aF3+aF4)];


    end
    output = [output, V4,{''}, A1];

end

