function [ normalized_output ] = MinMaxNormalization(input_vector)

    % Compute min-max normalization 0 - 1
    
    value_min = min(input_vector);
    value_max = max(input_vector);
    normalized_output = (input_vector-value_min)/(value_max-value_min);


end

