function [TI,alpha] = TIComputation(x,y)

    % Compute TI of 2 signal (x,y) from 1-20 Hz with 0.25 resolution
    % Compute Alpha coherence of 2 signal (x,y) from 8-12 Hz with 0.25 resolution
    % Using method of Welch estimation with no overlap, zeropadded to 4 sec
    
    % Outout
    %  - TI = numeric value
    %  - alpha = numberic value
    [cxy,~] = mscohere(x,y,hanning(128),0,[0:0.25:64],128);
    range = 1:0.25:20;
%     N = length(range);
%     TI_const = -1/N;
%     TI_sum = 0;
%     
%     % Compute TI
%     for i = range
%         position = (i*4)+1;
%         TI_sum = TI_sum + log(1-cxy(position));
%     end
%     TI = TI_const * TI_sum;

    % Compute Estimated TI
    TI = 0;
    for i = range
        position = (i*4)+1;
        TI = TI + cxy(position);
    end
    
    % Compute Alpha coherence
    range_alpha = 8:0.25:12;
    alpha = 0;
    for i = range_alpha
        position = (i*4)+1;
        alpha = alpha + cxy(position);
    end

end

