function output = powerbandsAnalysis(psdmatrix, bands)

    bands = bands + 1 ;

    avg_allepoch = mean(psdmatrix,1);
    avg_allepoch = squeeze(avg_allepoch); % matrix of [psd x electrode]
    
    output = [];
    for i = 1: (length(bands)/2)
        output = [output ;mean(avg_allepoch(  bands((i*2)-1):bands(i*2)  ,:),1)];
    end
end
